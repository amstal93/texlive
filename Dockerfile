ARG CI_REGISTRY
FROM debian:buster-backports

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install make inkscape texlive-full
